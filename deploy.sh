#python3 -m venv env
#chmod -R a+rwx env
#source env/bin/activate
#pip install -r requirements.txt
#python3 manage.py migrate
#python3 manage.py collectstatic --no-input
#deactivate
docker-compose up --build -d
docker-compose exec app python manage.py migrate
